# tezos-account-verify

Public code library to help developers authenticate Tezos tz1 addresses to ensure ownership of account without divulging private keys or making token transfers